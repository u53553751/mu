{-# LANGUAGE OverloadedStrings #-}

import Foreign.C.String

import Mu.API
import Mu.AST
import Mu.Impl.RefImpl2
import Mu.Interface

main :: IO ()
main = do
    gcOpts <- newCString "vmLog=DEBUG\nsosSize=524288\nlosSize=524288\nglobalSize=1048576\nstackSize=32768\n"
    putStrLn "Creating Micro VM"
    mew <- newMu' gcOpts

    putStrLn "Creating new context"
    ctx <- newContext mew

    withBuilder ctx $ do
        define (TypeDefinition "@aoeu" (MuInt 13))
        define (Constant "@konstantin" "@aoeu" (IntCtor 13))

    putStrLn "Closing the context"
    _ <- closeMu mew

    return ()
