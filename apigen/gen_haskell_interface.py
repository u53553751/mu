from collections import namedtuple
from typing import Dict

import sys

import pprint
import muapiparser


FunctionInfo = namedtuple('FunctionInfo', ['name', 'param_types', 'params', 'return_type'])


def print_indent(indent, value, **kwargs):
    print(('    '*indent) + value, **kwargs)

def slurp(fn):
    """Read the contents of a file into memory and dump the string.
    """
    with open(fn) as f:
        return f.read()


def pascalToCamel(string, startLowercase=True):
    """Turn a Pascal_Cased string into a CamelCase string with optional
    lowercased first letter.
    """
    if not string:
        return ""

    result = ""

    for word in string.lower().split('_'):
        if word:
            result += word[0].upper() + word[1:]

    if startLowercase:
        result = result[0].lower() + result[1:]

    return result


def function_info(method: Dict[str, str], context: str) -> FunctionInfo:
    sig = '{function_name} :: FunPtr ({type})'
    param_types = [translate(param['type']) for param in method['params']]

    # wrap the return type if necessary
    return_type = translate(method['ret_ty'])
    if ' ' in return_type:
        return_type = '(' + return_type + ')'

    # Need to handle these cases specially
    if method['name'] in ('load', 'name_of', 'id_of'):
        prefix = context[2:]
        name = '_' + prefix + '_' + method['name']
    else:
        name = '_' + method['name']

    return FunctionInfo(name=name,
                        param_types=param_types,
                        params=[p['name'] for p in method['params']],
                        return_type=return_type)


def translate(name):
    """Translate special cases of C type names to Haskell.
    """
    special_translations = {
        'char': 'CChar',
        'int8_t': 'CChar',
        'uint8_t': 'CChar',
        'int16_t': 'CShort',
        'uint16_t': 'CUShort',
        'int32_t': 'CInt',
        'uint32_t': 'CUInt',
        'int64_t': 'CLong',
        'uint64_t': 'CULong',
        'float': 'CFloat',
        'double': 'CDouble',
        'int': 'CInt',
        'intptr_t': 'CIntPtr',
        'uintptr_t': 'CUIntPtr',
        'void': '()',
        '_MuTrapHandler_Func*': 'FunPtr (Ptr MuCtx -> MuThreadRefValue -> MuStackRefValue -> MuWPID -> Ptr MuTrapHandlerResult -> Ptr MuStackRefValue -> Ptr (Ptr MuValue) -> Ptr MuArraySize -> Ptr MuValuesFreer -> Ptr MuCPtr -> Ptr MuRefValue -> MuCPtr -> IO ())',
        '_MuValuesFreer_Func*': 'FunPtr (Ptr MuValue -> MuCPtr -> IO ())',
        '_MuCFP_Func*': 'FunPtr (IO ())'
        }

    try:
        return special_translations[name]
    except KeyError:
        pass

    # if the type refers to one of the hidden internal values
    # if name[0] == '_':
    #     return '()'

    # if the type is actually an array (and all the special cases have been
    # handled earlier
    if name[-1] == '*':
        return 'Ptr ' + translate(name[:-1])

    return name


def print_enums(stuff):
    """Print related Haskell code for the enumerations defined in a Mu API.
    """
    print("-- | Enumerations: all ints, no type safety.")
    for enum in stuff['enums']:
        # fields = enum['defs']
        # data_fields = ' | '.join(field['name'] for field in enum['defs'])
        # print('data {enum_name}Enum = {fields}'.format(enum_name=enum_name,
        #                                            fields=data_fields))
        # print('instance MuFlagged {enum_name}Enum where'.format(enum_name=enum_name))

        for field in enum['defs']:
            field_name = pascalToCamel(field['name'], startLowercase=True)
            print("{} :: {}".format(field_name, enum['name']))
            print("{} = {}".format(field_name, field['value']))
        print()


def print_structs(stuff):
    """Print related Haskell code for the structs (and their methods) defined in
    a Mu API.

    This implementation exposes them as methods of a typeclass, in final tagless
    style.
    """
    for struct in stuff['structs']:
        functions = [function_info(m, struct['name']) for m in struct['methods']]
        function_outputs = ',\n'.join("{name} :: FunPtr ({arg_tys} -> IO {ret_ty})"
                                      .format(name=f.name,
                                              arg_tys=' -> '.join(f.param_types),
                                              ret_ty=f.return_type)
                                      for f in functions)

        print("""data {name} = {name} {{
_{name}_header :: Ptr (),
{functions}
}} deriving (Generic)

instance CStorable {name}

instance Storable {name} where
  sizeOf = cSizeOf
  alignment = cAlignment
  poke = cPoke
  peek = cPeek

""".format(name=struct['name'], functions=function_outputs))

        for function in functions:
            pattern = """foreign import ccall "dynamic" _run{pascal_name} :: Dynamic ({type} -> IO {ret_type})
{camel_name} :: MonadIO m => {type} -> m {ret_type}
{camel_name} {params} = liftIO $ do
    {struct_name} <- peek {first_param}
    _run{pascal_name} ({pascal_name} {struct_name}) {params}
{{-# INLINEABLE {camel_name} #-}}
"""

            print(pattern.format(pascal_name=function.name,
                                 type=' -> '.join(function.param_types),
                                 ret_type=function.return_type,
                                 camel_name=pascalToCamel(function.name, True),
                                 # first argument is encoded in monad
                                 first_param=function.params[0],
                                 params=' '.join(function.params),
                                 struct_name=pascalToCamel(struct['name'])))


def print_typedefs(stuff):
    """Translate Mu API C typedefs to Haskell.

    This implementation exposes them as Haskell type aliases.
    """
    for type_name, type_parameter in stuff['typedefs_order']:
        type_alias = 'type {type_name} = {type_parameter}'
        print(type_alias.format(type_name=type_name,
                                type_parameter=translate(type_parameter)))
    print()


def print_module_header():
    """Print the headers necessary for this Haskell file.
    """
    print("""-- | This file is automatically generated.
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE FlexibleContexts #-}
module Mu.API where

import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Reader (ReaderT, MonadReader, ask, runReaderT)

import Foreign
import Foreign.Ptr
import Foreign.C.Types
import GHC.Generics

import Foreign.CStorable

type Dynamic a = FunPtr a -> a

muNoID :: MuID
muNoID = 0
""")


def print_haskell_file(stuff):
    print_module_header()
    print_typedefs(stuff)
    print_enums(stuff)
    print_structs(stuff)

if __name__ == '__main__':
    stuff = muapiparser.parse_muapi(slurp(sys.argv[1]))
    print_haskell_file(stuff)
