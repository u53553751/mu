-- |
-- Module      :  Mu.Interface
-- Copyright   :  Author 2011-2012
-- License     :  BSD3
--
-- Maintainer  :  email@something.com
-- Stability   :  experimental
-- Portability :  unknown
--
-- A Haskell-level interface for the Mu Micro Virtual Machine.  Contains
-- Haskell-type-compatible functions for many operations that map directly to
-- the Mu itself.
--

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}

module Mu.Interface
    ( -- * Top-level interface
      buildBundle
    , define
      -- * Builder monad
    , Builder
    , withBuilder
    , query
    , queries
      -- * Utility functions
    , getID
    , withSymbol
    , withAnonymousSymbol
    , withSymbolIO
    ) where

import qualified Data.ByteString as B
import Data.Functor (void)
import Data.Monoid ((<>))
import Data.Maybe (isJust)
import Control.Monad.State (MonadState, StateT, evalStateT, gets)
import Control.Monad.Reader (MonadReader)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.List (genericLength)
import Foreign.C.String (newCString)
import qualified Foreign as F
import qualified Foreign.C as C
import qualified Data.Map.Strict as M

import Lens.Micro.Platform ((%=), Lens')

import Mu.API
import Mu.AST

import Debug.Trace


-------------------------------------------------- * Top-level interface

-- | Load the Haskell-level Mu 'Bundle' and transfer it to a new MicroVM
-- instance.
buildBundle :: F.Ptr MuCtx -> Bundle -> IO ()
buildBundle ctx bundle = withBuilder ctx $ mapM_ define (unBundle bundle)


doExpr :: Assigned Expression -> Builder MuFuncNode
doExpr (dests := expr) = do
    irb <- gets _irbPtr
    withAnonymousSymbol $ \sym -> case expr of
        BinaryOperation {} -> error "BinaryOperation not implemented"
        CompareOperation {} -> error "CompareOperation not implemented"
        ConvertOperation {} -> error "ConvertOperation not implemented"
        AtomicRMWOperation {} -> error "AtomicRMWOperation not implemented"
        CmpXchg {} -> error "CmpXchg not implemented"
        Fence {} -> error "Fence not implemented"
        New {} -> error "New not implemented"
        NewHybrid {} -> error "NewHybrid not implemented"
        Alloca {} -> error "Alloca not implemented"
        AllocaHybrid {} -> error "AllocaHybrid not implemented"
        Return retvals -> do
            (vals, n_vals) <- queries retvals
            newRet irb sym vals n_vals
        Throw {} -> error "Throw not implemented"
        Call {} -> error "Call not implemented"
        CCall {} -> error "CCall not implemented"
        TailCall {} -> error "TailCall not implemented"
        Branch1 {} -> error "Branch1 not implemented"
        Branch2 {} -> error "Branch2 not implemented"
        WatchPoint {} -> error "WatchPoint not implemented"
        Trap {} -> error "Trap not implemented"
        WPBranch {} -> error "WPBranch not implemented"
        Switch {} -> error "Switch not implemented"
        SwapStack {} -> error "SwapStack not implemented"
        NewThread {} -> error "NewThread not implemented"
        Comminst inst flags tys vars exception keepalive -> do
            -- TODO: flags not implemented
            (tyIDs, tyIDsSz) <- queries tys
            (varIDs, varIDsSz) <- queries vars
            (destIDs, destIDsSz) <- queries dests
            newComminst irb sym
                        destIDs destIDsSz
                        (fromEnum' inst)
                        F.nullPtr 0 -- flags
                        tyIDs tyIDsSz
                        F.nullPtr 0 -- sigs
                        varIDs varIDsSz
                        exception'
                        keepalive'
          where
            exception' = case exception of
                Nothing -> muNoID
                Just _ -> error "Comminst: exceptions not implemented"
            keepalive' = case keepalive of
                Nothing -> muNoID
                Just _ -> error "Comminst: keepalives not implemented"
        Load {} -> error "Load not implemented"
        Store isPtr memOrd ty dest val exc -> do
            tyID <- query ty
            destID <- query dest
            valID <- query val
            excID <- case exc of
                Nothing -> return muNoID
                Just _ -> error "Store: exceptions not implemented"
            newStore irb sym (bool isPtr) memOrd' tyID destID valID muNoID
          where
            memOrd' = case memOrd of
                Nothing -> muOrdNotAtomic
                Just ord -> fromEnum' ord
        ExtractValue {} -> error "ExtractValue not implemented"
        InsertValue {} -> error "InsertValue not implemented"
        ExtractElement {} -> error "ExtractValues not implemented"
        InsertElement {} -> error "InsertValues not implemented"
        ShuffleVector {} -> error "ShuffleVector not implemented"
        GetIRef ty from -> do
            tyID <- query ty
            fromID <- query from
            resID <- query $ head dests
            newGetiref irb sym resID tyID fromID
        GetFieldIRef isPtr ty index from -> do
            tyID <- query ty
            fromID <- query from
            resID <- query $ head dests  -- unsafe, I know
            newGetfieldiref irb sym resID (bool isPtr) tyID (fromIntegral index) fromID
        GetElemIRef {} -> error "GetElemIRef not implemented"
        ShiftIRef {} -> error "ShiftIRef not implemented"
        GetVarPartIRef {} -> error "GetVarPartIRef not implemented"


emitBB :: FunctionVerName -> BasicBlock -> Builder MuID
emitBB fvname (BasicBlock n params exc instr term) = do
    irb <- gets _irbPtr
    (paramIDs, _) <- toArray =<< mapM (newSymbol . fst) params
    (paramtyIDs, n_paramTyIDs) <- queries $ fmap snd params
    excID <- case exc of
        Just excParam -> newSymbol excParam
        Nothing -> return muNoID
    (insts, n_insts) <- toArray =<< mapM doExpr (instr ++ [[] := term])
    withSymbol n $ \sym ->
        newBb irb sym paramIDs paramtyIDs n_paramTyIDs excID insts n_insts


-- | Load a 'Definition' into the Mu itself.
define :: Definition -> Builder ()
define defn = do
    irb <- gets _irbPtr
    case defn of

        TypeDefinition n ty -> query n >>= \sym -> case ty of
            MuInt len -> newTypeInt irb sym (fromIntegral len)
            MuFloat -> newTypeFloat irb sym
            MuDouble -> newTypeDouble irb sym
            Ref refty -> do
                reftyID <- query refty
                newTypeRef irb sym reftyID
            IRef refty -> do
                reftyID <- query refty
                newTypeIref irb sym reftyID
            WeakRef refty -> do
                reftyID <- query refty
                newTypeWeakref irb sym reftyID
            UPtr refty -> do
                reftyID <- query refty
                newTypeUptr irb sym reftyID
            Struct tys -> do
                (tyIDs, count) <- queries tys
                newTypeStruct irb sym tyIDs count
            Array _ _ -> error "Array not implemented"
            Vector _ _ -> error "Vector not implemented"
            Hybrid fixedtys varty -> do
                (fixedtyIDs, n_fixedtyIDs) <- queries fixedtys
                vartyID <- query varty
                newTypeHybrid irb sym fixedtyIDs n_fixedtyIDs vartyID
            Void -> newTypeVoid irb sym
            ThreadRef -> newTypeThreadref irb sym
            StackRef -> newTypeStackref irb sym
            FrameCursorRef -> newTypeFramecursorref irb sym
            TagRef64 -> newTypeTagref64 irb sym
            FuncRef sig -> do
                sigID <- query sig
                newTypeFuncref irb sym sigID
            UFuncPtr sig -> do
                sigID <- query sig
                newTypeUfuncptr irb sym sigID

        SignatureDefinition n argtys rettys -> do
            (argtyIDs, n_argtyIDs) <- queries argtys
            (rettyIDs, n_rettyIDs) <- queries rettys
            query n >>= \sym ->
                newFuncsig irb sym argtyIDs n_argtyIDs rettyIDs n_rettyIDs
            return ()

        FunctionDefinition n ver sig entry body -> do
            sigID <- query sig
            funcID <- do
                sym <- query n
                newFunc irb sym sigID
                return sym
            (blockIDs, n_blockIDs) <- toArray =<< mapM (emitBB fvName) (entry:body)
            query fvName >>= \sym ->
                newFuncVer irb sym funcID blockIDs n_blockIDs
            return ()
          where
            fvName = versionedName n ver


        Constant n ty ctor -> do
            tyID <- query ty
            query n >>= \sym -> case ctor of
                IntCtor val -> newConstInt irb sym tyID (fromIntegral val)
                FloatCtor val -> newConstFloat irb sym tyID (C.CFloat val)
                DoubleCtor val -> newConstDouble irb sym tyID (C.CDouble val)
                ListCtor _ -> error "ListCtor not implemented"
                NullCtor -> newConstNull irb sym tyID
                ExternCtor str -> liftIO $ B.useAsCString str $ \cstr ->
                    newConstExtern irb sym tyID cstr

        GlobalCell n ty -> query n >>= \sym -> do
            tyID <- query ty
            newGlobalCell irb sym tyID

        ExposedFunction {} -> error "ExposedFunction not implemented"


-------------------------------------------------- * Monad helper

data BuilderState = BuilderState
    { _ids :: M.Map String MuID
    , _ctxPtr :: F.Ptr MuCtx
    , _irbPtr :: F.Ptr MuIRBuilder
    }

ids :: Lens' BuilderState (M.Map String MuID)
ids f (BuilderState i cp ip) = fmap (\i' -> BuilderState i' cp ip) (f i)

newtype Builder a = Builder (StateT BuilderState IO a)
    deriving (Functor, Applicative, Monad, MonadState BuilderState, MonadIO)

withBuilder :: F.Ptr MuCtx -> Builder () -> IO ()
withBuilder ctx (Builder m) = do
    irb <- newIrBuilder ctx
    evalStateT m (BuilderState M.empty ctx irb)
    irbuilderLoad irb

-- | Look up the value of a name.  If it doesn't exist, create it.
query :: HasName a => a -> Builder MuID
query n = do
    knownIDs <- gets _ids
    case M.lookup (nameOf n) knownIDs of
        Nothing -> newSymbol n
        Just i -> return i

queries :: HasName a => [a] -> Builder (F.Ptr MuID, MuArraySize)
queries ns = do
    ctx <- gets _ctxPtr
    ids <- mapM query ns
    toArray ids

remember :: HasName a => a -> MuID -> Builder ()
remember n i = ids %= M.insert (nameOf n) i


-------------------------------------------------- * Utility functions

toArray :: (F.Storable a, MonadIO m) => [a] -> m (F.Ptr a, MuArraySize)
toArray xs = do
    arr <- liftIO (F.newArray xs)
    return (arr, genericLength xs)


-- | Extract the 'MuID' of an object via its name as a @String@.
getID :: (HasName a, MonadIO m) => F.Ptr MuCtx -> a -> m (MuID)
getID ctx n = liftIO $ C.newCString (nameOf n) >>= ctxIdOf ctx


-- | Declare a symbol in the Mu.
newSymbol :: HasName a => a -> Builder MuID
newSymbol n = do
    irb <- gets _irbPtr
    n' <- liftIO (C.newCString (nameOf n))
    sym <- genSym irb n'
    remember n sym
    return sym


-- | Declare a symbol in the Mu, and return that symbol's ID.
withSymbol :: HasName a => a -> (MuID -> Builder b) -> Builder MuID
withSymbol n f = do
    irb <- gets _irbPtr
    n' <- liftIO (C.newCString (nameOf n))
    sym <- genSym irb n'
    _ <- f sym
    remember n sym
    return sym

-- | Declare a symbol in the Mu without a name attached to it, and return that
-- symbol's ID.
withAnonymousSymbol :: (MuID -> Builder b) -> Builder MuID
withAnonymousSymbol f = do
    irb <- gets _irbPtr
    sym <- genSym irb F.nullPtr
    result <- f sym
    return sym

-- | Generalised version of 'withSymbol'.
withSymbolIO
    :: (HasName a, MonadIO m)
    => F.Ptr MuIRBuilder -> a -> (MuID -> m b) -> m MuID
withSymbolIO irb n f = do
    n' <- liftIO (C.newCString (nameOf n))
    sym <- genSym irb n'
    _ <- f sym
    return sym

bool :: Bool -> MuBool
bool b = case b of
    True -> 1
    False -> 0

fromEnum' :: Enum a => a -> C.CUInt
fromEnum' = fromIntegral . fromEnum
