{-# LANGUAGE ForeignFunctionInterface #-}
module Mu.Impl.RefImpl2
    ( newMu
    , newMu'
    , closeMu
    ) where

import Data.Functor (void)
import Foreign.Ptr
import Foreign.C.Types

import Mu.API (MuVM)

foreign import ccall "refimpl2-start.h mu_refimpl2_new" newMu
    :: IO (Ptr MuVM)
foreign import ccall "refimpl2-start.h mu_refimpl2_new_ex" newMu'
    :: Ptr CChar -> IO (Ptr MuVM)
foreign import ccall "refimpl2-start.h mu_refimpl2_close" _close_mu
    :: Ptr MuVM -> IO (Ptr MuVM)

closeMu :: Ptr MuVM -> IO ()
closeMu = void . _close_mu
